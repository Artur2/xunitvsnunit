﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xUnitvsNUnit.Products;

namespace xUnitvsNUnit.Fixtures
{
    public class ProductFixture : IDisposable
    {
        private Random _random;

        public ProductFixture()
        {
            _random = new Random();
        }

        public Random Random => _random;

        public Product GetDummyProduct()
        {
            return new Product();
        }

        public Product GetSampleProductWithRandomPrice()
        {
            return new Product()
            {
                Id = 1,
                Name = "Product",
                Price = (decimal) Random.NextDouble()
            };
        }

        public Product GetSampleProductWithRandomPriceAndRandomId()
        {
            return new Product()
            {
                Id = Random.Next(1),
                Name = "Product",
                Price = (decimal)Random.NextDouble()
            };
        }

        public void Dispose()
        {
            
        }
    }
}
