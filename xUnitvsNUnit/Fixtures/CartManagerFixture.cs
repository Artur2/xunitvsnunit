﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xUnitvsNUnit.Order;
using xUnitvsNUnit.Products;

namespace xUnitvsNUnit.Fixtures
{
    public class CartManagerFixture : IDisposable
    {
        public CartManager GetEmptyCart()
        {
            return new CartManager();
        }

        public CartManager GetCartWithRandomProducts()
        {
            var cartManager = GetEmptyCart();

            cartManager.AddProduct(new Product()
            {
                Id = 1,
                Price = 100,
                Name = "Product1"
            }, 1);

            cartManager.AddProduct(new Product()
            {
                Id = 2,
                Price = 300,
                Name = "Product2"
            }, 1);

            cartManager.AddProduct(new Product()
            {
                Id = 3,
                Price = 400,
                Name = "Product3"
            }, 1);

            return cartManager;
        }

        public void Dispose()
        {
            // Release anything 
        }
    }
}
