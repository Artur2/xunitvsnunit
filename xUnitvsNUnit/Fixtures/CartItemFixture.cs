﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xUnitvsNUnit.Order;
using xUnitvsNUnit.Products;

namespace xUnitvsNUnit.Fixtures
{
    public class CartItemFixture : IDisposable
    {
        public CartItem GetDummyCartItem()
        {
            return new CartItem();
        }

        public CartItem WrapProduct(Product product, int quantity = 0)
        {
            return new CartItem
            {
                ProductId = product.Id,
                Price = product.Price
            };
        }

        public void Dispose()
        {
            
        }
    }
}
