﻿using System;
using System.Collections.Generic;
using System.Linq;
using xUnitvsNUnit.Fixtures;
using xUnitvsNUnit.Order;
using xUnitvsNUnit.Products;
using Xunit;
using Xunit.Abstractions;

namespace xUnitvsNUnit
{
    // xUnit following "data-driven" testing methodology
    // There is no things like TestFixture for 
    // xUnit writen by author of NUnit v2.*
    // xUnit starting by default in parallel and use one instance of test class and fixtures per method
    [Collection("Unit")]
    public class CartManagerTests :
        IClassFixture<CartManagerFixture>,
        IClassFixture<ProductFixture>,
        IClassFixture<CartItemFixture>,
        IDisposable // Responsible for teardown(free of unused resources or smthng like that)
    {
        private CartManagerFixture _cartManagerFixture;
        private ProductFixture _productFixture;
        private CartItemFixture _cartItemFixture;
        private ITestOutputHelper _testOutputHelper;

        // SetUp method for NUnit
        // Here we can use power from one or more fixtures for responsibility segregation 
        public CartManagerTests(
            CartManagerFixture cartManagerFixture,
            ProductFixture productFixture,
            CartItemFixture cartItemFixture,
            ITestOutputHelper outputHelper)
        {
            _cartManagerFixture = cartManagerFixture;
            _productFixture = productFixture;
            _cartItemFixture = cartItemFixture;
            _testOutputHelper = outputHelper;
        }

        [Fact]
        public void CartManager_Should_return_Empty_List_Of_Items()
        {
            var cartManager = _cartManagerFixture.GetEmptyCart();
            _testOutputHelper.WriteLine("Checking up empty cart items, they should be not null, but empty.");
            Assert.Empty(cartManager.GetCartItems());
        }

        [Fact]
        public void CartManager_AddProduct_Should_Throw_AppropriateException()
        {
            var cartManager = _cartManagerFixture.GetEmptyCart();

            _testOutputHelper.WriteLine("Checking up exceptions with wrong data passed to method.");
            Assert.Throws<ArgumentNullException>(() => cartManager.AddProduct(null, 1));
            Assert.Throws<InvalidOperationException>(() => cartManager.AddProduct(_productFixture.GetDummyProduct(), 0));
            Assert.Throws<InvalidOperationException>(() => cartManager.AddProduct(_productFixture.GetDummyProduct(), -1));
        }

        [Fact]
        public void CartManager_RemoveProduct_Shouldnot_Throw_When_We_Remove_Not_Existing_Product()
        {
            var cartManager = _cartManagerFixture.GetEmptyCart();
            var product = _productFixture.GetSampleProductWithRandomPriceAndRandomId();

            cartManager.RemoveProduct(product.Id, 1);
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(2, 3)]
        [InlineData(3, 4)]
        public void CartManager_Should_Return_Added_Product_Correctly(int productId, int quantity)
        {
            // Arrange
            var cartManager = _cartManagerFixture.GetEmptyCart();
            var product = _productFixture.GetDummyProduct();
            product.Id = productId;
            var cartItem = _cartItemFixture.GetDummyCartItem();
            cartItem.Quantity = quantity;
            cartItem.Price = product.Price;
            cartItem.ProductId = product.Id;

            // Act
            cartManager.AddProduct(product, quantity);

            // Assert
            Assert.Collection(cartManager.GetCartItems(), item =>
            {
                Assert.Equal(cartItem.Quantity, item.Quantity);
                Assert.Equal(cartItem.Price, item.Price);
                Assert.Equal(cartItem.ProductId, item.ProductId);
            });
        }

        [Theory]
        [InlineData(1, 6, 10)]
        [InlineData(2, 3, 10)]
        [InlineData(2, 6, 10)]
        public void CartManager_Should_Remove_Product_Correctly(int productId, int quantity, int initialQuantity)
        {
            // Arrange
            var cartManager = _cartManagerFixture.GetEmptyCart();
            var product = _productFixture.GetDummyProduct();
            product.Id = productId;
            var cartItem = _cartItemFixture.GetDummyCartItem();
            cartItem.ProductId = productId;
            cartItem.Quantity = initialQuantity;
            var expectedQuantity = initialQuantity - quantity;
            if (expectedQuantity < 0)
            {
                expectedQuantity = 0;
            }

            // Act
            cartManager.AddProduct(product, initialQuantity);
            cartManager.RemoveProduct(productId, quantity);

            // Assert
            Assert.Collection(cartManager.GetCartItems(), _cartItem =>
            {
                Assert.True(_cartItem.Quantity == expectedQuantity);
            });
        }

        [Theory]
        [InlineData(1, 0)]
        [InlineData(1, -2)]
        public void CartManager_Remove_Product_Should_Correct_Throw_Exceptions(int initialQuantity, int removingQuantity)
        {
            var cartManager = _cartManagerFixture.GetEmptyCart();
            var product = _productFixture.GetSampleProductWithRandomPriceAndRandomId();

            cartManager.AddProduct(product, initialQuantity);

            Assert.Throws<ArgumentOutOfRangeException>(() => cartManager.RemoveProduct(product.Id, removingQuantity));
        }

        [Fact]
        public void CartManager_Should_Correct_Show_Properties_Price()
        {
            // Arrange
            var cartManager = _cartManagerFixture.GetEmptyCart();
            var product = _productFixture.GetSampleProductWithRandomPrice();

            // Act
            cartManager.AddProduct(product, 1);

            // Assert
            Assert.Collection(cartManager.GetCartItems(), cartItem =>
            {
                Assert.Equal(cartItem.Price, product.Price);
            });
        }

        public static IEnumerable<object> ProductsWithDifferentIds()
        {
            var items = Enumerable.Range(0, 10)
                .Aggregate(new List<Product>(), (seed, i) =>
                {
                    var product = new Product { Id = i + 1 };

                    seed.Add(product);
                    return seed;
                });

            yield return new object[]
            {
                items.ToArray()
            };
        }

        [Theory]
        [MemberData(nameof(ProductsWithDifferentIds))]
        public void CartManager_Should_Correct_Add_Multiple_Products(Product[] productsToAdd)
        {
            // Arrange
            var cartManager = _cartManagerFixture.GetEmptyCart();

            // Act
            foreach (var product in productsToAdd)
            {
                cartManager.AddProduct(product, 1);
            }

            // Assert
            Assert.NotEmpty(cartManager.GetCartItems());
            Assert.Collection(cartManager.GetCartItems().Take(2), 
            cartItem =>
            {
                Assert.NotSame(0, cartItem.ProductId);
            },
            cartItem =>
            {
                Assert.NotSame(0, cartItem.ProductId);
            });
        }

        [Theory]
        [MemberData(nameof(ProductsWithDifferentIds))]
        public void CartManager_Should_Correct_Refresh_CartItems(Product[] initialProducts)
        {
            // Arrange
            var cartManager = _cartManagerFixture.GetCartWithRandomProducts();
            var asserts = initialProducts.Take(3).Aggregate(new List<Action<CartItem>>(), (seed, product) =>
            {
                Action<CartItem> action = (cartItem) => Assert.Equal(product.Price, cartItem.Price);
                seed.Add(action);

                return seed;
            }).ToArray();

            // Act
            cartManager.RefreshCart(initialProducts.Take(3));

            // Assert
            // Verifying all items with one or more condition
            Assert.Collection(cartManager.GetCartItems(), asserts);
        }

        // Teardown method in NUnit, for closing connection or something like that
        public void Dispose()
        {
            _cartManagerFixture.Dispose();
            _productFixture.Dispose();
            _cartManagerFixture.Dispose();
        }
    }
}
