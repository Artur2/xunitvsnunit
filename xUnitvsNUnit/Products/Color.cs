﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xUnitvsNUnit.Products
{
    public class Color
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal? AdditionalPrice { get; set; }
    }
}
