﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xUnitvsNUnit.Products;

namespace xUnitvsNUnit.Order
{
    public class CartManager
    {
        private IList<CartItem> _cartItems = new List<CartItem>();

        public void AddProduct(Product product, int quantity)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (quantity <= 0)
            {
                throw new InvalidOperationException("Quantity must be greater than zero");
            }

            var existingProductCartItem = _cartItems.FirstOrDefault(x => x.ProductId == product.Id);
            if (existingProductCartItem != null)
            {
                existingProductCartItem.Quantity += quantity;
            }
            else
            {
                _cartItems.Add(new CartItem()
                {
                    Quantity = quantity,
                    ProductId = product.Id,
                    Price = product.Price
                });
            }
        }

        public void RemoveProduct(int productId, int quantity)
        {
            if (!_cartItems.Any())
            {
                return;
            }

            var existingItem = _cartItems.FirstOrDefault(x => x.ProductId == productId);
            if (existingItem != null)
            {
                if (existingItem.Quantity == quantity)
                {
                    _cartItems.Remove(existingItem);
                }
                else if(quantity > 0 && (existingItem.Quantity - quantity) > 0)
                {
                    existingItem.Quantity -= quantity;
                }
                else if (quantity <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(quantity), "quantity must be greater than 0");
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(quantity), "Quantity must be not greater than in cart");
                }
            }
        }

        public void RefreshCart(IEnumerable<Product> productsToRefresh)
        {
            if (!_cartItems.Any())
            {
                return;
            }

            foreach (var product in productsToRefresh)
            {
                var existingProduct = _cartItems.FirstOrDefault(x => x.ProductId == product.Id);
                if (existingProduct == null)
                {
                    continue;
                }

                existingProduct.Price = product.Price;
            }
        }

        public IEnumerable<CartItem> GetCartItems()
        {
            return _cartItems;
        }
    }
}
