﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xUnitvsNUnit.Order
{
    public class CartItem
    {
        public Guid TransientId { get; set; } = Guid.NewGuid();

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public int ColorId { get; set; }
    }
}
