﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xunit;

namespace xUnitvsNUnit
{
    public class NumbersTests : IDisposable
    {
        [Theory]
        [InlineData(1)]
        [InlineData(0)]
        [InlineData(-1)]
        public void Divide_by_zero_should_throw_exception(int b)
        {
            var a = 1;
            if (b <= 0)
            {
                b = 1;
            }

            var c = a - b;

            Assert.Throws<DivideByZeroException>(() => a / c);
        }

        public int SharedInt { get; set; }

        [Fact]
        public void Increment_should_work_correct()
        {
            SharedInt++;

            Assert.Equal(1, SharedInt);
        }

        [Fact]
        public async Task Async_Divide()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(100));
            var c = 1;
            var b = 1;
            Assert.Equal(1, c / b);
        }

        public static IEnumerable<object[]> TaskDelayData()
        {
            yield return new object[] { -1  };
            yield return new object[] { int.MaxValue };
        }

        [Theory]
        [MemberData(nameof(TaskDelayData))]
        public async Task Task_Delay_Should_Throw_Exception(int delay)
        {
            var ts = new TimeSpan(0, 0, delay);

            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await Task.Delay(ts));
        }

        public void Dispose()
        {

        }
    }
}
